document.addEventListener("DOMContentLoaded", function () {
    const calcularButton = document.getElementById("calcular");
    calcularButton.addEventListener("click", calcularPulsaciones);
});

function calcularPulsaciones() {
    const edadInput = document.getElementById("edad");
    const resultadoDiv = document.getElementById("resultado");

    const edad = parseInt(edadInput.value);

    let pulsaciones_promedio;

    if (edad >= 0 && edad <= 1) {
        pulsaciones_promedio = (140 + 160) / 2;
    } else if (edad >= 2 && edad <= 12) {
        pulsaciones_promedio = (110 + 115) / 2;
    } else if (edad >= 13 && edad <= 40) {
        pulsaciones_promedio = (70 + 80) / 2;
    } else {
        pulsaciones_promedio = (60 + 70) / 2;
    }

    const pulsaciones_totales = pulsaciones_promedio * 365.25 * 24 * 60; // minutos en un año

    resultadoDiv.innerHTML = `Pulsaciones promedio ${edad} años son: ${pulsaciones_promedio} por minuto.`;
    resultadoDiv.innerHTML += `<br><br>Las pulsaciones totales en un año son: ${pulsaciones_totales}.`;
}