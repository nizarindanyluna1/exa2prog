let arregloCalif = new Array(35);


let alumnosReprobados=0;
let alumnosAprobados=0;

let reprobadosPromedio=0.0;
let aprobadosPromedio=0.0;
let promedioGeneral=0.0;

let tabla = document.getElementById('tabla');

let btnGenerar = document.getElementById('generar');

let lblAlumnosReprobados = document.getElementById('cantidadReprobados');
let lblAlumnosAprobados = document.getElementById('cantidadAprobados');

let lblPromedioReprobados = document.getElementById('promedioReprobados');
let lblPromedioAprobados = document.getElementById('promedioAprobados');
let lblPromedioGeneral = document.getElementById('promedioGeneral');
//-------------------
btnGenerar.addEventListener('click', function() {
    for (let i = 0; i < arregloCalif.length; i++) {
        arregloCalif[i] = (Math.random() * 10).toFixed(0);
        if (arregloCalif[i] < 7) {
            alumnosReprobados = alumnosReprobados + 1;
            reprobadosPromedio = reprobadosPromedio + parseFloat(arregloCalif[i]);
        }
        if (arregloCalif[i] >= 7) {
            alumnosAprobados = alumnosAprobados + 1;
            aprobadosPromedio = aprobadosPromedio + parseFloat(arregloCalif[i]);
        }

        promedioGeneral = promedioGeneral + parseFloat(arregloCalif[i]);

        let fila = tabla.insertRow();
        let celdaCalif = fila.insertCell(0);
        celdaCalif.innerHTML = arregloCalif[i];
    }

    if (alumnosAprobados > 0) {
        aprobadosPromedio = (aprobadosPromedio / alumnosAprobados).toFixed(2);
    } else {
        aprobadosPromedio = 0.0;
    }
    
    if (alumnosReprobados > 0) {
        reprobadosPromedio = (reprobadosPromedio / alumnosReprobados).toFixed(2);
    } else {
        reprobadosPromedio = 0.0;
    }

    promedioGeneral = (promedioGeneral / arregloCalif.length).toFixed(2);

    lblAlumnosReprobados.innerHTML = alumnosReprobados;
    lblAlumnosAprobados.innerHTML = alumnosAprobados;
    lblPromedioReprobados.innerHTML = reprobadosPromedio;
    lblPromedioAprobados.innerHTML = aprobadosPromedio;
    lblPromedioGeneral.innerHTML = promedioGeneral;

    console.log("Promedio de aprobados:", aprobadosPromedio);
    console.log("Promedio de reprobados:", reprobadosPromedio);
    console.log("Promedio general:", promedioGeneral);
});

//-------------------------

let btnLimpiar = document.getElementById('limpiar');

btnLimpiar.addEventListener('click', function(){
    var rows = tabla.querySelectorAll('tr');
  for (var i = 1; i < rows.length; i++) {
    rows[i].remove();
  }
    lblAlumnosReprobados.innerHTML= "";
    lblAlumnosAprobados.innerHTML= "";
    lblPromedioReprobados.innerHTML= "";
    lblPromedioAprobados.innerHTML= "";
    lblPromedioGeneral.innerHTML= "";

    alumnosReprobados = 0;
    alumnosAprobados = 0;
    reprobadosPromedio = 0.0;
    aprobadosPromedio = 0.0;
    promedioGeneral = 0.0;
})