let btnEnviar = document.getElementById('enviar');

btnEnviar.addEventListener('click',  function(){
  var nombre = document.getElementById('nombre').value;
  var edad = document.getElementById('edad').value;
  var sexo = document.querySelector('input[name="sexo"]:checked').value;
  var comentarios = document.getElementById('comentarios').value;

  var cuerpoCorreo = 'Nombre: ' + nombre + '%0AEdad: ' + edad + '%0ASexo: ' + sexo + '%0AComentarios: ' + comentarios;
  window.location.href = 'mailto:microinformaticamx@gmail.com?subject=Formulario de Contacto&body=' + cuerpoCorreo;
})
//-----
let btnLimpiar =document.getElementById('limpiar');


btnLimpiar.addEventListener('click', function () {
    document.getElementById('nombre').value = '';
    document.getElementById('edad').value = '';
    var sexoRadios = document.querySelectorAll('input[name="sexo"]');
    sexoRadios.forEach(function (radio) {
        radio.checked = false;
    });
    document.getElementById('comentarios').value = '';
});